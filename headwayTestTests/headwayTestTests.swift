//
//  headwayTestTests.swift
//  headwayTestTests
//
//  Created by Aliona Kostenko on 06.11.2023.
//

import XCTest
@testable import headwayTest

final class headwayTestTests: XCTestCase {
    
    func prepareDataForState(action: AudioPlayerAction, currentTime: TimeInterval = 0) -> AudioPlayerState{
        var state = AudioPlayerState()
        state.currentTime = currentTime
        let audioURL = Bundle.main.url(forResource: "summary", withExtension: "mp3")!
        let environment = AudioPlayerEnvironment(audioURL: audioURL)
        let _ = audioPlayerReducer(&state, action, environment)
        return state
    }
    
    func testAudioPlayerReducer_PlayAction() {
        let state = prepareDataForState(action: .play)
        
        XCTAssertEqual(state.isPlaying, true)
    }
    
    func testAudioPlayerReducer_PauseAction() {
        let state = prepareDataForState(action: .pause)
        
        XCTAssertEqual(state.isPlaying, false)
    }
    
    func testAudioPlayerReducer_ForwardAction() {
        let state = prepareDataForState(action: .fastForward)
        
        XCTAssertEqual(state.currentTime, 10)
    }
    
    func testAudioPlayerReducer_RewindAction() {
        let state = prepareDataForState(action: .rewind, currentTime: 5)
        
        XCTAssertEqual(state.currentTime, 0)
    }
    
    func testUpdateTimeAction() {
        var state = AudioPlayerState()
        let audioURL = Bundle.main.url(forResource: "summary", withExtension: "mp3")!
        let environment = AudioPlayerEnvironment(audioURL: audioURL)
        environment.audioPlayer.currentTime = 30.0
        
        let _ = audioPlayerReducer(&state, .updateTime, environment)
        
        XCTAssertEqual(state.currentTime, 30.0)
    }
    
    func testSetCustomAction() {
        var state = AudioPlayerState()
        let audioURL = Bundle.main.url(forResource: "summary", withExtension: "mp3")!
        let environment = AudioPlayerEnvironment(audioURL: audioURL)
        environment.audioPlayer.currentTime = 0.0
        
        let _ = audioPlayerReducer(&state, .setCustomTime(30), environment)
        
        XCTAssertEqual(state.currentTime, 30.0)
    }
    
    func testPreviousAction() {
        var state = AudioPlayerState()
        let audioURL = Bundle.main.url(forResource: "summary", withExtension: "mp3")!
        let environment = AudioPlayerEnvironment(audioURL: audioURL)
        state.sections = [AudioSection(id: 1, startTime: 0.0, endTime: 10.0, title: "THE FIVE LOVE LANGUAGES. SUMMARY")]
        state.currentTime = 30
        let _ = audioPlayerReducer(&state, .selectPreviousSection, environment)
        
        XCTAssertEqual(state.currentTime, 0.0)
    }
    
    func testNextAction() {
        var state = AudioPlayerState()
        let audioURL = Bundle.main.url(forResource: "summary", withExtension: "mp3")!
        let environment = AudioPlayerEnvironment(audioURL: audioURL)
        state.sections = [AudioSection(id: 2, startTime: 10.0, endTime: 78.0, title: "INTRODUCTION")]
        state.currentTime = 0
        let _ = audioPlayerReducer(&state, .selectNextSection, environment)
        
        XCTAssertEqual(state.currentTime, 10)
    }
    
    func testPreviousWhenFirstAction() {
        var state = AudioPlayerState()
        let audioURL = Bundle.main.url(forResource: "summary", withExtension: "mp3")!
        let environment = AudioPlayerEnvironment(audioURL: audioURL)
        state.sections = [AudioSection(id: 1, startTime: 0.0, endTime: 10.0, title: "THE FIVE LOVE LANGUAGES. SUMMARY")]
        state.currentTime = 9
        let _ = audioPlayerReducer(&state, .selectPreviousSection, environment)
        
        XCTAssertEqual(state.currentTime, 0.0)
    }
    
    func testToogleAction() {
        var state = AudioPlayerState()
        let audioURL = Bundle.main.url(forResource: "summary", withExtension: "mp3")!
        let environment = AudioPlayerEnvironment(audioURL: audioURL)
        state.playbackSpeed = 1
        let _ = audioPlayerReducer(&state, .togglePlaybackSpeed, environment)
        
        XCTAssertEqual(environment.audioPlayer.rate, 1.5)
    }
    
    func testToogle1Action() {
        var state = AudioPlayerState()
        let audioURL = Bundle.main.url(forResource: "summary", withExtension: "mp3")!
        let environment = AudioPlayerEnvironment(audioURL: audioURL)
        state.playbackSpeed = 1.5
        let _ = audioPlayerReducer(&state, .togglePlaybackSpeed, environment)
        
        XCTAssertEqual(environment.audioPlayer.rate, 2)
    }
    
    func testToogle2Action() {
        var state = AudioPlayerState()
        let audioURL = Bundle.main.url(forResource: "summary", withExtension: "mp3")!
        let environment = AudioPlayerEnvironment(audioURL: audioURL)
        state.playbackSpeed = 2
        let _ = audioPlayerReducer(&state, .togglePlaybackSpeed, environment)
        
        XCTAssertEqual(environment.audioPlayer.rate, 1)
    }
}
