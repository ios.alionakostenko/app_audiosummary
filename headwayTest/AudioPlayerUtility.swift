//
//  AudioPlayerUtility.swift
//  headwayTest
//
//  Created by Aliona Kostenko on 07.11.2023.
//

import Foundation

func formatTime(_ time: TimeInterval) -> String {
       let minutes = Int(time) / 60
       let seconds = Int(time) % 60
       return String(format: "%02d:%02d", minutes, seconds)
   }

func parseSectionsFromJSONFile() async throws -> [AudioSection] {
    guard let url = Bundle.main.url(forResource: "sections", withExtension: "json"),
          let data = try? Data(contentsOf: url),
          let sections = try? JSONDecoder().decode([String: [AudioSection]].self, from: data),
          let sectionArray = sections["sections"] else {
        throw NSError(domain: "YourAppDomain", code: 400, userInfo: [NSLocalizedDescriptionKey: "Failed to parse JSON data"])
    }

    return sectionArray
}
