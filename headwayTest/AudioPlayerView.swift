//
//  AudioPlayerView.swift
//  headwayTest
//
//  Created by Aliona Kostenko on 07.11.2023.
//

import SwiftUI
import ComposableArchitecture
import AVFoundation
import Combine

struct AudioPlayerView: View {
    let store: Store<AudioPlayerState, AudioPlayerAction>
    @State var timer: AnyCancellable?
    @State private var sliderValue: Double = 0
    
    var body: some View {
        WithViewStore(store) { viewStore in
            ZStack {
                Color("BackgroundColor").edgesIgnoringSafeArea(.all)
                VStack {
                    Image("bookImage")
                        .cornerRadius(10)
                    if let currentSection = viewStore.currentSection {
                        Text("KEY POINT \(currentSection.id) OF 11")
                        Text(currentSection.title)
                            .font(Font.system(size: 16, weight: .light))
                    }
                    HStack(spacing: 5) {
                        Text("\(formatTime(viewStore.currentTime))")
                            .frame(width: 50)
                        Slider(value: $sliderValue, in: 0...1191, onEditingChanged: { editing in
                            if !editing {
                                viewStore.send(.setCustomTime(sliderValue))
                            }
                        })
                        .frame(width: 200)
                        Text("\(formatTime(1191 - viewStore.currentTime))")
                            .frame(width: 50)
                    }
                    .padding(.horizontal, 15)
                    Button {
                        viewStore.send(.togglePlaybackSpeed)
                    } label: {
                        ZStack {
                            Color("speedColor")
                                .frame(width: 110, height: 40)
                            Text("Speed x\(String(viewStore.playbackSpeed))")
                        }
                            .cornerRadius(10)
                    }
                    HStack(alignment: .center, spacing: 20) {
                        Button(action: {
                            viewStore.send(.selectPreviousSection)
                        }) {
                            Image("previousChapter")
                        }
                        Button(action: {
                            viewStore.send(.rewind)
                        }) {
                            Image("secondsBack")
                        }
                        Button(action: {
                            viewStore.send(viewStore.isPlaying ? .pause : .play)
                        }) {
                            Image(viewStore.isPlaying ? "pauseButton" : "playButton")
                        }
                        Button(action: {
                            viewStore.send(.fastForward)
                        }) {
                            Image("secondsForward")
                        }
                        Button(action: {
                            viewStore.send(.selectNextSection)
                        }) {
                            Image("nextChapter")
                        }
                    }
                }
            }
            .onAppear {
                timer = Timer.publish(every: 1, on: .main, in: .common)
                    .autoconnect()
                    .sink { _ in
                        viewStore.send(.updateTime)
                        sliderValue = viewStore.currentTime
                    }
                Task {
                    do {
                        let sections = try await parseSectionsFromJSONFile()
                        viewStore.send(.parseSections(sections))
                    } catch {
                        print("Error parsing sections from JSON: \(error)")
                    }
                }
            }
        }
    }
}
