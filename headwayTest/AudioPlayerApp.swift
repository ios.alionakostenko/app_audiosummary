//
//  AudioPlayerApp.swift
//  headwayTest
//
//  Created by Aliona Kostenko on 06.11.2023.
//

import SwiftUI
import ComposableArchitecture
import AVFoundation

@main
struct AudioPlayerApp: App {
    let audioURL = Bundle.main.url(forResource: "summary", withExtension: "mp3")!
    let audioPlayer: AVAudioPlayer
    let store: Store<AudioPlayerState, AudioPlayerAction>

    init() {
        self.audioPlayer = AVAudioPlayer()
        self.store = Store(
            initialState: AudioPlayerState(),
            reducer: audioPlayerReducer,
            environment: AudioPlayerEnvironment(audioURL: audioURL)
        )
    }

    var body: some Scene {
        WindowGroup {
            AudioPlayerView(store: store)
        }
    }
}
