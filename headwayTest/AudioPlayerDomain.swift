//
//  AudioPlayerDomain.swift
//  headwayTest
//
//  Created by Aliona Kostenko on 06.11.2023.
//

import SwiftUI
import ComposableArchitecture
import AVFoundation
import Combine

struct AudioPlayerState: Equatable {
    var isPlaying = false
    var playbackSpeed = 1.0
    var currentTime: TimeInterval = 0
    var sections: [AudioSection] = []
    var currentSection: AudioSection? {
        return sections.first { $0.startTime <= currentTime && $0.endTime >= currentTime }
    }
}

struct AudioSection: Decodable, Equatable {
    let id: Int
    let startTime: TimeInterval
    let endTime: TimeInterval
    let title: String
}

enum AudioPlayerAction: Equatable {
    case play
    case pause
    case fastForward
    case rewind
    case togglePlaybackSpeed
    case selectNextSection
    case selectPreviousSection
    case setCustomTime(TimeInterval)
    case updateTime
    case parseSections([AudioSection])
}

let audioPlayerReducer = Reducer<AudioPlayerState, AudioPlayerAction, AudioPlayerEnvironment> { state, action, environment in
    switch action {
    case .play:
        state.isPlaying = true
        environment.audioPlayer.play()
        return .none
        
    case .pause:
        state.isPlaying = false
        environment.audioPlayer.pause()
        return .none
        
    case .fastForward:
        let newTime = min(state.currentTime + 10, environment.audioPlayer.duration)
        state.currentTime = newTime
        environment.audioPlayer.currentTime = newTime
        return .none
        
    case .rewind:
        let newTime = max(state.currentTime - 5, 0)
        state.currentTime = newTime
        environment.audioPlayer.currentTime = newTime
        return .none
    case .selectNextSection:
        if let nextSection = state.sections.first(where: { $0.startTime > state.currentTime }) {
            state.currentTime = nextSection.startTime
            environment.audioPlayer.currentTime = nextSection.startTime
        } else {
            state.currentTime = 0.0
            environment.audioPlayer.currentTime = 0.0
        }
        return .none
        
    case .selectPreviousSection:
        if let previousSection = state.sections.last(where: { $0.endTime < state.currentTime }) {
            state.currentTime = previousSection.startTime
            environment.audioPlayer.currentTime = previousSection.startTime
        } else {
            state.currentTime = 0.0
            environment.audioPlayer.currentTime = 0.0
        }
        return .none
    case let .setCustomTime(time):
        environment.audioPlayer.currentTime = time
        state.currentTime = time
        return .none
    case .togglePlaybackSpeed:
        switch state.playbackSpeed {
        case 1.0:
            state.playbackSpeed = 1.5
        case 1.5:
            state.playbackSpeed = 2
        default:
            state.playbackSpeed = 1
        }
        environment.audioPlayer.rate = Float(state.playbackSpeed)
        return .none
    case .updateTime:
        state.currentTime = environment.audioPlayer.currentTime
        return .none
    case let .parseSections(sections):
        state.sections = sections
        return .none
    }
}

struct AudioPlayerEnvironment {
    var audioPlayer: AVAudioPlayer

    init(audioURL: URL) {
        do {
            self.audioPlayer = try AVAudioPlayer(contentsOf: audioURL)
            self.audioPlayer.prepareToPlay()
            self.audioPlayer.enableRate = true
        } catch {
            fatalError("Не вдалося ініціалізувати AVAudioPlayer")
        }
    }
}

